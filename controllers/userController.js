const User = require('../models/User');
const Course = require('../models/Course');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Check if the email already exists
/*
  Steps
  1. Use mongoose find method to find duplicate emails
  2. Use the then method to send a response back to the fe applciation based on the result of the find method

*/
module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then(result => {
    if (result.length > 0)
      return true
    else
      return false
  })
};

// User registration
/*
  1. create new user object
  2. encrypt password
  3. save to db
*/
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
  });

  return newUser.save().then((user, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return true;
    }
  });

};

// User controller
/*

  1. Check db if email exists
  2. Verify supplied password vs db password
  3. Generate and return jwt and return false if not

*/
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then(result => {
    if (!result)
      return false
    else {
      const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) }
      } else {
        return false;
      }
    }
  })
};

//// Get user details
//module.exports.getProfile = (reqBody) => {
  //return User.findById(reqBody.id).then((user, err) => {
    //if (err) {
      //console.log(err);
      //return false
    //}

    //if (user) {
      //user.password = '';
      //return user;
    //} else {
      //return false;
    //}
  //});
//};

module.exports.getProfile = (data) => {
  return User.findById(data.userId).then(result => {
    result.password = '';
    return result
  })
};

// Enroll user to class
/*
  1. Find doc in db using user id
  2. add courseid to user's enrollment []
  3. update document in db
*/

/*
  1. find user who is enrolling and update enrollments subdocument array.
  push the courseid in the rollments array

  2. Find course where we are enrolling and update its enrollees
  push userId in enrollees array

  since we will access 2 collections in one action, we will have to wait for completion of the action instead of letting javascript continue line per line.

  to wait for the result of a function, use the await keyword
*/
module.exports.enroll = async (data) => {
  let isUserUpdated = await User.findById(data.userId).then(user => {
    user.enrollments.push({ courseId: data.courseId });
    return user.save().then((user, error) => {
      if (error) {
        return false
      } else {
        return true
      }
    });
  });

  let isCourseUpdated = await Course.findById(data.courseId).then(course => {
    course.enrollees.push({ userId: data.userId });
    return course.save().then((course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  });

  if (isUserUpdated && isCourseUpdated) {
    return true;
  } else {
    return false;
  }
};
