const Course = require('../models/Course');

// Create new course

// create
/*

  1. Create new Course object using mongoose model and the information from the
  2. save new user to database

*/
//module.exports.addCourse = (requestBody) => {
//if (!requestBody.isAdmin) {
//const c = Course.find()
//return c.then((result, err) => false );
//}

//const newCourse = new Course(requestBody.course);

//return newCourse.save().then((course, error) => {
//return true;
//});
//};

module.exports.addCourse = (data) => {
  console.log(data);
  if (data.isAdmin) {
    let newCourse = new Course({
      name: data.course.name,
      description: data.course.description,
      price: data.course.price,
    });

    console.log(newCourse);
    return newCourse.save().then((course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
  } else {
    return false;
  }
};

// retrieve all courses
/*

  1. Retrieve all the courses from the database: Model.find({})
*/

module.exports.getAllCourses = () => {
  return Course.find({}).then((result) => {
    return result;
  });
};

module.exports.getActiveCourses = () => {
  return Course.find({ isActive: true }).then((result) => {
    return result;
  });
};

module.exports.getCourse = (courseId) => {
  return Course.findById(courseId).then((result) => {
    return result;
  });
};

module.exports.updateCourse = (reqParams, reqBody) => {
  let updatedCourse = {
    name: reqBody.name,
    descrpition: reqBody.description,
    price: reqBody.price,
  };
  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
    (course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// Archive course
module.exports.archiveCourse = (courseId) => {
  return Course.findByIdAndUpdate(courseId, { isActive: false })
    .then((course, err) => {
      if (err)
        return false
      else
        return true
  });
};
