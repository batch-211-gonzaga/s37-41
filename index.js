const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

const app = express();

const PORT = process.env.PORT || 4000;

// MongoDB
mongoose.connect(
   'mongodb+srv://admin123:admin123@project0.csykcns.mongodb.net/s37-41?retryWrites=true&w=majority',
  {
    useNewUrlParser: true, // compatibility
    useUnifiedTopology: true, // compatibility
  }
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB error'));
db.once('open', () => console.log('Connected to MongoDB'));

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// routes
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
