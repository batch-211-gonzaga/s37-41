const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// Check for checking if the user's email already exists in the db
// pass the 'body' property of our request

router.post('/checkEmail', (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((result) => res.send(result));
});


// User registration
router.post('/register', (req, res) => {
  userController.registerUser(req.body)
    .then(result => res.send(result));
})

// User authentication
router.post('/login', (req, res) => {
  userController.loginUser(req.body).then(result => res.send(result));
});


//// Get user details
//router.post('/details', (req, res) => {
  //userController.getUserDetails(req.body).then(result => res.send(result));
//});

router.get('/details', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log('userData', userData);

  userController.getProfile({ userId: userData.id }).then(result => res.send(result));
});

router.post('/enroll', auth.verify, (req, res) => {
  //const userData = auth.decode(req.headers.authorization);
  //console.log('##### 00 ', userData);

  //if (userData.id !== req.body.userId) {
    //res.send(false);
    //return;
  //}

  let data = {
    userId: auth.decode(req.headers.authorization).id,
    courseId: req.body.courseId
  };
  userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;
