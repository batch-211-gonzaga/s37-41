const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require('../auth');

// Create a course
// refactor to implement user authentication
router.post('/', auth.verify, (req, res) => {
  const data = {
    course: req.body,
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };

  console.log(data);
  courseController.addCourse(data).then(result => res.send(result))
});

// Route for retrieveing all courses
router.get('/all', (req, res) => {
  courseController.getAllCourses().then(resultfromController => res.send(resultfromController));
});

// retrieve all active courses
router.get('/', (req, res) => {
  courseController.getActiveCourses().then(resultfromController => res.send(resultfromController));
});

// retrieve course by id
router.get('/:courseId', (req, res) => {
  courseController.getCourse(req.params.courseId).then(resultfromController => res.send(resultfromController));
});

// update a course
router.put('/:courseId', auth.verify, (req, res) => {
  isAdmin = auth.decode(req.headers.authorization).isAdmin

  courseController.updateCourse(req.params,req.body).then(result => res.send(result))
});

// archive a course
router.put('/:courseId/archive', auth.verify, (req, res) => {
  isAdmin = auth.decode(req.headers.authorization).isAdmin

  if (!isAdmin) {
    res.send(false);
    return;
  }

  courseController
    .archiveCourse(req.params.courseId)
    .then(result => res.send(result))
});

module.exports = router;
