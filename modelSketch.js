/*
App: Booking System API

Scenario:
  A course booking system application where a  user can enroll into a course

Type: Course Booking System (Web App)

Description
  A course booking system application where a user can enroll into a course
  Allows an admin to do CRUD operations
  Allows users to register into our database

Features:
  - User Login (User Authentication)
  - User Registration

  Customer/Authenticated Users:
    - View Courses (All active)
    - Enroll

  Admin Users:
    - Add Course
    - Update
    - Archive/Unarchive a course (soft delete)
    - View Courses (All courses, active/inactive)
    - View/Manage User Accounts

  All User (guests, customers, admin)
    - View Active Courses

*/

// Data Model for the Booking System

// Two-way Embedding

user {
  id, // - unique id,
  firstName,
  lastName,
  email,
  password,
  mobileNumber,
  isAdmin,
  enrollment: [
    {
      id, // - document identifier,
      courseid,
      enrolledOn - Date new Date()
      status - String default: Enrolled
    }
  ]
}

course {
  id,
  name,
  description,
  price,
  slots,
  isActive
  createdOn,
  enrollees: [
    {
      id,
      userId,
      iPaid,
      dateEnrolled
    }
  ]
}

// Referencing

user {
  id - unique id,
  firstName,
  lastName,
  email,
  password,
  mobileNumber,
  isAdmin,
}

course {
  id,
  name,
  description,
  price,
  slots,
  isActive
  createdOn,
}

enrollment {
  id,
  userId,
  courseId,
  isPaid,
  dateEnrolled
}


/*
  1. Create a models folder and create a 'course.js file' to store the course schema
  2. Put the necessary data types of our fields
  3. Create the Course Model with its Proper schema
*/
