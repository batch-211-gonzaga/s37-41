const jwt = require('jsonwebtoken');

const secret = 'CourseBookingAPI'

// JWTs
// header
// payload
// signature

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };

  return jwt.sign(data, secret, {});
};

// Token verification
/*
  Receive gift and open the lock to verify if the sender is legitimate and the gift was not tampered with
*/
module.exports.verify = (req, res, next) => {
  // the token is retrieved from the request header
  // this can be provided in postman uder
    // Authorization > Bearer Token
  let token = req.headers.authorization;

  if (typeof token !== 'undefined') {
    console.log(token);
    token = token.slice(7, token.length);
    // the slice method takes only the token from the information sent via request header
    // Bearer 98ab0we8cf
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return res.send({ auth: 'failed' });
      } else {
        // allow the app to proceed with the next middleware function/callback function
        next();
      }
    })
  } else {
    return res.send({ auth: 'failed' });
  }
};


// Token decryption
/*
    open the gift and get the contents
*/
module.exports.decode = (token) => {
  if (typeof token !== 'undefined') {
    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {
        return jwt.decode(token, { complete: true }).payload
      }
    });
  }
  else {
    return null
  }
}
